# For Centos 7: disable firewalld and install/enable/start iptables and
# ip6tables

package 'iptables-services'

execute 'systemctl mask firewalld' do
  not_if "systemctl status firewalld | grep 'Loaded: masked'"
  notifies :run, 'execute[systemctl stop firewalld]'
end

execute 'systemctl stop firewalld' do
  action :nothing
end

%w(iptables ip6tables).each do |svc|
  execute "systemctl enable #{svc}" do
    not_if "systemctl status #{svc} | grep 'Loaded: .*enabled)'"
  end

  execute "systemctl start #{svc}" do
    not_if "systemctl status #{svc} | grep 'Active: active'"
  end
end
