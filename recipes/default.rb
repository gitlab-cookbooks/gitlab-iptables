#
# Cookbook:: gitlab-iptables
# Recipe:: default
# License:: MIT
#
# Copyright:: (C) 2016 GitLab Inc.
#
def normalize(text)
  text_dup = text.dup
  text_dup.downcase!
  text_dup.tr(' ', '_')
end

# If we are on AWS, don't bother with iptables/ufw. This behavior can
# be overruled if node['gitlab-iptables']['enable'] == true
enable_iptables = node['gitlab-iptables']['enable'] ||
                  !AwsHelper.aws?

unless enable_iptables
  include_recipe 'ufw::disable' if platform_family?('debian')
  return
end

if platform_family?('rhel') && node['platform_version'] =~ /^7\./
  include_recipe 'gitlab-iptables::iptables-instead-of-firewalld'
end

# IPv4 rules
iptables_ng_rule '1-ssh-allow' do
  rule '--protocol tcp --dport 22 -j ACCEPT'
  ip_version 4
end

# Allow rule for specific src/dest then reject rule for any
rule_nr = 10
firewall_rules = nil
firewall_rules = node['firewall']['rules'] if node['firewall']
firewall_rules ||= []
firewall_rules.each do |rule|
  rule.each do |rule_name, rule_spec|
    source = "--source #{rule_spec['source']}" if rule_spec['source']
    destination = "--destination #{rule_spec['destination']}" if rule_spec['destination']
    protocols = rule_spec['protocol'] ? [rule_spec['protocol']] : %w(tcp udp)
    protocols.each do |protocol|
      iptables_ng_rule "#{rule_nr}-#{normalize(rule_name)}-#{protocol}-allow" do
        rule "--protocol #{protocol} #{source} #{destination} --dport #{rule_spec['port']} -j ACCEPT"
        ip_version 4
      end
      rule_nr += 1
    end
  end
end

iptables_ng_rule '998-log-before-reject' do
  rule '-j LOG --log-level debug --log-prefix "Reject unmatched packet " -m limit --limit 1/second --limit-burst 10'
  ip_version 4
end

# If not on Azure drop all traffic by default
azure = false
azure = true if node['kernel']['modules'].include?('hid_hyperv')

unless azure
  iptables_ng_rule '1-lo-allow' do
    rule '-i lo -j ACCEPT'
    ip_version 4
  end
  iptables_ng_rule '1-icmp-allow' do
    rule '-p icmp -j ACCEPT'
    ip_version 4
  end
  iptables_ng_rule '1-state-rel-est-allow' do
    rule '-m state --state RELATED,ESTABLISHED -j ACCEPT'
    ip_version 4
  end
  iptables_ng_rule '999-all-drop' do
    rule '-j REJECT'
    ip_version 4
  end
end

# IPv6 rules
iptables_ng_rule '999-all-drop-ip6' do
  rule '-j REJECT'
  ip_version 6
end

# Restart docker service which uses own iptables chains
if File.exist?('/etc/init.d/docker') || File.exist?('/usr/lib/systemd/system/docker.service')
  service 'docker' do
    if platform?('ubuntu')
      if node['platform_version'].to_f >= 9.10 && node['platform_version'].to_f < 16.04
        provider Chef::Provider::Service::Upstart
      end
    end
    action :restart
    not_if 'iptables -L DOCKER'
  end
end
