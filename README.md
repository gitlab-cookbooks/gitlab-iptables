# gitlab-iptables cookbook

This cookbook converts firewall rules in the style of https://github.com/opscode-cookbooks/ufw .
Note that we only support a subset of the syntax of the UFW cookbook.
For instance, 'deny' rules and port ranges are not supported at the moment.

This cookbook wraps [iptables-ng](https://supermarket.chef.io/cookbooks/iptables-ng), translating
legacy firewall rules specified in the format used by the UFW cookbook (i.e. using the attribute set
`['firewall']['rules']`).

## Usage

In your node's or role's `run_list`, include recipe `gitlab-iptables::default`, and specify the list of
rules in its attributes list as described below.

Note: By default, this cookbook is configured to do nothing in AWS.  To enable it on an AWS VM, you must
also explicitly set the node attribute `['gitlab-iptables']['enable']` to `true`.

## Example

Specifying rules in a Chef role file looks like this:

```
{
  "default_attributes": {
    "firewall": {
      "rules": [
        {
          "Allow nginx": {
            "port": "443"
          }
        },
        {
          "Allow Postgres only from allowed clients": {
            "source": "10.11.12.0/24,99.99.99.99",
            "port": "5432",
            "protocol": "tcp"
          }
        }
      ]
    }
  },
  "run_list": [
    "recipe[gitlab-iptables]"
  ]
}
```

And the resulting iptables rules would look like this (newlines added for grouping clarity):

```
$ sudo cat /etc/iptables/rules.v4 
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
--append INPUT -p icmp -j ACCEPT
--append INPUT -i lo -j ACCEPT
--append INPUT --protocol tcp --dport 22 -j ACCEPT
--append INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

--append INPUT --protocol tcp --dport 443 -j ACCEPT
--append INPUT --protocol tcp --dport 443 -j DROP

--append INPUT --protocol udp --dport 443 -j ACCEPT
--append INPUT --protocol udp --dport 443 -j DROP

--append INPUT --protocol tcp --source 10.11.12.0/24,99.99.99.99 --dport 5432 -j ACCEPT
--append INPUT --protocol tcp --dport 5432 -j DROP

--append INPUT -j DROP
COMMIT

*mangle
:PREROUTING ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:INPUT ACCEPT [0:0]
COMMIT

*nat
:PREROUTING ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
*raw
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
```


## Rule structure and attributes

Structure of a rule:
`{ <rule_name>: <rule_spec> }`

The "rule_name" is an arbitrary string briefly describing the intent of the rule.
The "rule_spec" is a set of attributes defining the matching criteria for the rule.

Attributes of a "rule_spec" object:
* port:         Required.  Matches the packet's destination port number to the specified value.
* source:       Optional.  Matches the packet's source IP address to the specified IP or netblock.
* destination:  Optional.  Matches the packet's destination IP address to the specified IP or netblock.
* protocol:     Optional.  Matches the packet's protocol to the single specified protocol name.  Default: "tcp" and "udp".


## Behavior

Manages the set of rules defined by the iptables "filter" tables's `INPUT` chain.  Other tables and chains are unmanaged.

All specified rules are implicitly IPv4 `ACCEPT` rules.

For each given rule_spec, the following set of rules is created:
* an ACCEPT rule for the complete rule_spec: port, protocol, source, destination
* a DROP rule for just: port, protocol
Note that this DROP rule is redundant with the implicit catch-all DROP rule, described later.

The list of rules is created in the given order.

Additionally, the following implicit rules are always added at a higher priority than the caller's specified rules:
* Accept TCP port 22 (ssh).
* Accept all traffic on the loopback network interface.
* Accept ICMP traffic.
* Accept packets for existing TCP/UDP connections.

And the following implicit rules are always added at a lower priority than the caller's specified rules:
* Drop all incoming IPv4 traffic not explicitly allowed by the preceding higher-priority rules.
* Drop all IPv6 traffic.

**WARNING:** Currently auto-pruning is *not* enabled, so unmanaged rules are *not* automatically removed, either
from runtime or the rules-persistence file (/etc/iptables/rules.v4).  This is dangerous, as it could leave
residual unintended unmanaged rules, and these rules may potentially reappear after a reboot.  We should consider
enabling auto-pruning after an audit of existing rules.
